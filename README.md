# Ant Design Pro

## 安装依赖

```bash
npm install
```

或者

```bash
yarn
```

### 启动项目

```bash
npm start
```

### 打包

```bash
npm run build
```

### 代码检查

```bash
npm run lint
```

You can also use script to auto fix some lint error:

```bash
npm run lint:fix
```

### 测试

```bash
npm test
```
